﻿public static class Constants
{
    public const string FigureTag = "Figure";
    public const string RotationMarkTag = "RotationMark";
    public const string HorizontalMarkName = "HorizontalMark";
    public const string GroundMaskName = "Ground";
}
