﻿using UnityEngine;

public class FiguresModule
{
    Figure[] figures;
    Figure activeFigure;

    public FiguresModule()
    {
        figures = GameObject.FindObjectsOfType<Figure>();
    }

    public void SelectFigure(GameObject obj)
    {
        if (obj == null)
        {
            if (activeFigure != null)
            {
                activeFigure.SetActive();
            }
        }
        else if (obj.tag == Constants.FigureTag)
        {
            if (activeFigure != null)
                activeFigure.SetSleep();

            activeFigure = obj.GetComponent<Figure>();
            activeFigure.SetMove();
        }
        else if (obj.tag == Constants.RotationMarkTag)
        {
            if (activeFigure != null)
                activeFigure.SetRotate(obj.name == Constants.HorizontalMarkName);
        }
    }

    public void ManipulateFigure(Vector3 posA, Vector3 posB)
    {
        if (activeFigure != null)
        {
            activeFigure.Manipulate(posA, posB);
        }
    }
}

