﻿using UnityEngine;

public class GameRoot : MonoBehaviour
{
    UserInput userInput;
    FiguresModule figuresModule;

    void Start()
    {
        figuresModule = new FiguresModule();

        userInput = new UserInput();
        userInput.Selected += OnSelected;
        userInput.Dragged += OnDragged;
    }

    void Update()
    {
        userInput.Refresh();
    }

    void OnSelected(GameObject obj)
    {
        figuresModule.SelectFigure(obj);
    }

    void OnDragged(Vector3 posA, Vector3 posB)
    {
        figuresModule.ManipulateFigure(posA, posB);
    }
}
