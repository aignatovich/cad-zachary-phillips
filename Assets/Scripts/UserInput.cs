﻿using System;
using UnityEngine;

public class UserInput
{
    public event Action<GameObject> Selected;
    public event Action<Vector3, Vector3> Dragged;

    Vector3 prevPos;
    bool mousePressed;
    int groundMask;

    public UserInput()
    {
        groundMask = LayerMask.GetMask(Constants.GroundMaskName);
    }

    public void Refresh()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousePressed = true;
            prevPos = Vector3.zero;

            RaycastHit hit1;
            Ray ray1 = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray1, out hit1))
            {
                Selected?.Invoke(hit1.transform.gameObject);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            mousePressed = false;

            Selected?.Invoke(null);
        }

        if (mousePressed)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask))
            {
                if (prevPos == Vector3.zero)
                {
                    prevPos = hit.point;
                    return;
                }

                if (prevPos != hit.point)
                {
                    Dragged?.Invoke(prevPos, hit.point);
                    prevPos = hit.point;
                }
            }
        }
    }
}
