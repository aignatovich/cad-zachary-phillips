﻿using UnityEngine;

public class TransformationKeeper : MonoBehaviour
{
    Vector3 position;
    Quaternion rotation;

    void Awake()
    {
        position = transform.localPosition;
        rotation = transform.rotation;
    }

    void OnEnable()
    {
        transform.position = transform.parent.position + position;
        transform.rotation = rotation;
    }

    void OnDisable()
    {
        transform.localPosition = Vector3.zero;
    }
}
