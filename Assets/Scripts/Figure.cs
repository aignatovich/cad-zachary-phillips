﻿using UnityEngine;

public class Figure : MonoBehaviour
{
    enum State
    {
        Sleep,
        Active,
        Move,
        Rotate
    }

    [SerializeField]
    GameObject horizontalMark;
    [SerializeField]
    GameObject verticalMark;

    bool isHorizontalRotation;
    bool isFirstRotation;
    Vector3 rotationVector;
    Quaternion startRotation;

    LineRenderer line;
    State state;

    void Start()
    {
        state = State.Sleep;
        ActivateMarks(false);
        CreateLine();
    }

    #region Change state
    public void SetSleep()
    {
        state = State.Sleep;
        ActivateLine(false);
        ActivateMarks(false);
    }

    public void SetActive()
    {
        state = State.Active;
        ActivateLine(false);
        ActivateMarks(true);
    }

    public void SetMove()
    {
        state = State.Move;
        ActivateLine(false);
        ActivateMarks(true);
    }

    public void SetRotate(bool isHorizontalRotation)
    {
        state = State.Rotate;

        this.isHorizontalRotation = isHorizontalRotation;
        isFirstRotation = true;

        ActivateLine(true);
        ActivateMarks(false);
    }
    #endregion

    #region Change position and rotation
    public void Manipulate(Vector3 posA, Vector3 posB)
    {
        switch (state)
        {
            case State.Move:
                Move(posA, posB);
                break;
            case State.Rotate:
                Rotate(posA, posB);
                break;
            default:
                break;
        }
    }

    void Rotate(Vector3 posA, Vector3 posB)
    {
        if (isFirstRotation)
        {
            isFirstRotation = false;
            rotationVector = posA - transform.position;
            rotationVector.z = 0;
            startRotation = transform.rotation;
            ActivateMarks(false);
            return;
        }

        var secondRotationVector = posB - transform.position;
        secondRotationVector.z = 0;
        var result = Vector3.SignedAngle(rotationVector, secondRotationVector, Vector3.forward);
        transform.rotation = startRotation;

        if (isHorizontalRotation)
        {
            transform.Rotate(0, 0, result, Space.World);
        }
        else
        {
            transform.Rotate(0, result, 0, Space.World);
        }

        DrawLine(posB);
    }

    void Move(Vector3 posA, Vector3 posB)
    {
        var step = posB - posA;
        step.z = 0;
        transform.position += step;
    }
    #endregion

    #region Decorations
    void ActivateMarks(bool activate)
    {
        horizontalMark.gameObject.SetActive(activate);
        verticalMark.gameObject.SetActive(activate);
    }

    void ActivateLine(bool activate)
    {
        line.gameObject.SetActive(true);
        line.SetPosition(0, transform.position);
        line.SetPosition(1, transform.position);
    }

    void CreateLine()
    {
        GameObject lineObject = new GameObject("Line");
        lineObject.transform.parent = transform;
        lineObject.transform.localPosition = Vector3.zero;
        line = lineObject.AddComponent<LineRenderer>();
        line.startColor = Color.red;
        line.startWidth = 0.1f;
        lineObject.gameObject.SetActive(false);
    }

    void DrawLine(Vector3 pos)
    {
        line.SetPosition(0, transform.position);
        line.SetPosition(1, pos);
    }
    #endregion
}
